package com.smaato.challenge.controller;

import com.smaato.challenge.utility.SmaatoUtility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import com.smaato.challenge.service.SmaatoService;

@RestController
@RequestMapping("/api/smaato")
public class SmaatoController {
    public static final String OK = "ok";
    public static final String FAILED = "failed";

    @Autowired
    private SmaatoService smaatoService;

    @Autowired
    private SmaatoUtility smaatoUtility;

    @GetMapping("/accept")
    public Mono<String> getAcceptStatus(@RequestParam(value = "id") Integer id, @RequestParam(value = "endpoint", required = false) String endpoint) {
        if (endpoint == null) {
            return Mono.just(id)
                    .map(requestId -> {
                        smaatoService.processRequestId(requestId);
                        return OK;
                    });
        } else {
            if (!smaatoUtility.isValidURL(endpoint)){
                return Mono.just(FAILED);
            }
            smaatoService.callToEndpoint(id, endpoint);
            return Mono.just(OK);

        }
    }
}


