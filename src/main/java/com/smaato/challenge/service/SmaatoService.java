package com.smaato.challenge.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.concurrent.ConcurrentSkipListSet;

@Service
public class SmaatoService {
    private static final Logger LOGGER = LogManager.getLogger(SmaatoService.class);
    private ConcurrentSkipListSet<Integer> uniqueCalls = new ConcurrentSkipListSet();
    private WebClient webClient = WebClient.create();

    public void processRequestId(Integer id) {
        uniqueCalls.add(id);
    }


    public void callToEndpoint(Integer id, String endpoint) {
        processRequestId(id);
        URI endpointUri = UriComponentsBuilder.fromHttpUrl(endpoint).queryParam("id", uniqueCalls.stream().count()).build().toUri();
        webClient.get()
                .uri(endpointUri)
                .exchangeToMono(clientResponse -> {
                    LOGGER.info("HTTP Response Code: {}",clientResponse.rawStatusCode());
                    return Mono.just(clientResponse);
                }).subscribe();
    }


    @Scheduled(fixedRate = 60000)
    public void writeNumberOfUniqueRequests() {
        LOGGER.info("Number of unique calls: {} ", uniqueCalls.stream().count());
        uniqueCalls.removeAll(uniqueCalls);
    }

}
