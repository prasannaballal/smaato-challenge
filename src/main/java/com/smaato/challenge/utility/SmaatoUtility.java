package com.smaato.challenge.utility;

import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class SmaatoUtility {

    public boolean isValidURL(String url) {
        try {
            UriComponentsBuilder.fromHttpUrl(url);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
