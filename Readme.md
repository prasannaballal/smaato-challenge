# Smaato coding challenge application

#Tech Stack used for this project
* Java 11
* Spring Boot 2.6.0
* Maven


# Getting Started
* Go to project root directory system_path/challenge
* Run ```mvn clean install``` this will generate the ```challenge-1.0.jar``` file in system_path/challenge/target.
* Run the jar.
* Application runs on port 8080 (for core problem and extension1).
* After the jar is running GET endpoint ```/api/smaato/accept ``` will be availbale.

#Assumptions
* If the invalid URI is sent as query param, it will be considered as error in processing and will not be counted as unique request.

#Extension
* **Extension 1**
  * For Extension 1 of the challenge please checkout the ```extension1``` branch and follow the steps in **Getting Started** section.
  * This will make the post call to the endpoint provided in query param.
* **Extension 2** 
  * For Extension 2 you will also have to download and start the ```api-gateway``` project shared in another repo.
  * For Extension 2 of the challenge please checkout the ```extension2``` branch from current ```challeenge``` application and follow the steps in **Getting Started** section.
  * This time application runs on port: ```8081```.

#Note
* A postman collection is also shared over email to test the different scenarios of the application.

